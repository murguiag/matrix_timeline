from setuptools import setup, find_packages
with open('README.rst', encoding='UTF-8') as f:
 readme = f.read()
setup(
 name='test_matrix',
 version='0.1.0',
 description=' Get Test results and visiualize in web page',
 long_description=readme,
 author='Guillermo',
 author_email='guillermo.murguia_morales@nokia.com',
 packages=find_packages('src'),
 package_dir={'': 'src'},
 install_requires=[],
 entry_points={
 'console_scripts': [
 'test_matrix=test_matrix.cli:main',
 ],
 }

)
