from argparse import Action, ArgumentParser
import pdb

known_visualization = ['yes', 'no']

class ViewAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        # pdb.set_trace()
        view, = values
        if view.lower() not in known_visualization:
            parser.error("Unknown view. Available views are 'yes' & 'no'")
        namespace.view = view.lower()

def create_parser():
    parser = ArgumentParser(description="""
    Parse xml test result into db and create a visualization.
    """)
    parser.add_argument("url", help="URL of database")
    parser.add_argument("--file", help="file need to specified", nargs=1, required=True)
    parser.add_argument("--view",
    help="visualization yes/no",
    nargs=1,
    action=ViewAction,
    required=True)
    # pdb.set_trace()
    return parser