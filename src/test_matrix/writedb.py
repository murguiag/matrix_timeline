from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from .models import db, testcasedb, testcasedb_descr  
import os
import pdb



def writetodb(testcases):
    app = Flask(__name__) 
    app.config.from_mapping(SECRET_KEY=os.environ.get('SECRET_KEY', default='dev'),)
    app.config.from_pyfile('config.py', silent=True)
    db.init_app(app)
    with app.app_context():
         for index, item  in enumerate(testcases):
             testpass = item["pass"]
             testfail = item["fail"]
             name = item["name"]
             date = item["date"]
             record = testcasedb(name=name,testpass=testpass,testfail=testfail,date=date)
             db.session.add(record)
             db.session.commit()

def write_to_db_test_descr_only(testcases):
    app = Flask(__name__) 
    app.config.from_mapping(SECRET_KEY=os.environ.get('SECRET_KEY', default='dev'),)
    app.config.from_pyfile('config.py', silent=True)
    db.init_app(app)
    with app.app_context():
         for index, item  in enumerate(testcases):
             name = item["name"]
             record = testcasedb_descr(name=name)
             db.session.add(record)
             db.session.commit()


#  Delete to be added later.        
        # # delete record
        # db.session.query(testcasedb).filter_by(id=2).delete()
        # db.session.query(testcasedb).filter_by(date='2021-12-07 17:16:05.749').delete()
        # db.session.query(testcasedb).filter_by(date='2021-12-09 17:16:11.292').delete()
        # db.session.commit()
