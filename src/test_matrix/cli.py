from argparse import Action, ArgumentParser
import pdb

known_drivers = ['db', 'json', 'csv']

class ViewAction(Action):
    def __call__(self, parser, namespace, values, option_string=None):
        # pdb.set_trace()
        driver, = values
        if driver.lower() not in known_drivers:
            parser.error("Unknown option. You have the following options: 'db' or 'json' or 'csv")
        namespace.driver = driver.lower()

def create_parser():
    parser = ArgumentParser(description="""
    Parse test suite names and pass/failure status from robot framework output xml.
    Then writes all the information into a postgres database.
    If user specifies view equal to yes, it generates an html page.
    """)
    parser.add_argument("-u", "--url", help="URL of database", nargs=1, required=True)
    parser.add_argument("-f", "--file", help="Output.xml file from robot framework", nargs=1, required=True)
    parser.add_argument("-utt", "--update_test_descr", help="Enable if you want only to update the test table descriptions to the latest", nargs=1, required=True)
    parser.add_argument("-d", "--driver",
    help="Take actions like write to db, json and or csv",
    nargs=1,
    action=ViewAction,
    required=False)
    # pdb.set_trace()
    return parser


def main():
    from test_matrix import xmlparse, writedb
    args = create_parser().parse_args()
    testcases = xmlparse.computexml(args.file)
    print ("We have succesfully parsed the ouput xml file")
    # pdb.set_trace()
    if args.driver == 'db':
        writedb.writetodb(testcases)
        print ("We have succesfully stored in DB the test results")
    elif args.driver == 'csv':
        print ("We will write the output in a csv format later....")
    elif args.driver == 'json':
        print ("We will write the output in a json format later....")
    if args.update_test_descr[0] == 'yes':
        writedb.write_to_db_test_descr_only(testcases)
        print ("We have succesfully updated the test descriptions")
    else:
        print ("We do NOT update the test descriptions!!!!")

    
 
    
