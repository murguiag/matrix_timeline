
from flask_sqlalchemy import SQLAlchemy
from mistune import markdown

db = SQLAlchemy()
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(200))
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, server_default=db.func.now(), server_onupdate=db.func.now())
    notes = db.relationship('Note', backref='author', lazy=True)

class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200))
    body = db.Column(db.Text)
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, server_default=db.func.now(), server_onupdate=db.func.now())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    @property
    def body_html(self):
        return markdown(self.body)
class testcasedb(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    testpass = db.Column(db.Integer)
    testfail = db.Column(db.Integer)
    date = db.Column(db.DateTime)
    release = db.Column(db.Integer)
    release_vsd = db.Column(db.Integer)
    release_vsc = db.Column(db.Integer)
    release_nsg = db.Column(db.Integer)
    # user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    # note_id = db.Column(db.Integer, db.ForeignKey('note.id'), nullable=True)

class testcasedb_descr(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    description = db.Column(db.String(200))



