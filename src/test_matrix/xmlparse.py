import xml.etree.ElementTree as ET
import pdb
import re



def computexml(xmlfile):
    # pdb.set_trace()
    xmlfile = str(xmlfile[0])
    tree = ET.parse(xmlfile)
    root = tree.getroot()
    testcases = []
    pattern = '^s\d{0,1}-s\d{0,3}$'
    test = {}
    for child1 in root:
        if child1.tag == 'statistics':
            # print (f"the child1 tag is : {child1.tag}")
            for child2 in child1:
                if child2.tag == 'suite':
                    # print (f"the child2 tag is: {child2.tag}")
                    for child3 in child2:
                        # pdb.set_trace()
                        if child3.tag == 'stat' and re.match(pattern, child3.attrib['id']):
                            # print (f"the child3 text is: {child3.text}")
                            # print (f"the child3 attrib is: {child3.attrib}")
                            # print (f"the date is: {root.attrib['generated']}")  
                            test['name'] = child3.attrib['name']
                            test['pass'] = child3.attrib['pass']
                            test['fail'] = child3.attrib['fail']
                            test['id'] = child3.attrib['id']
                            test['date'] = root.attrib['generated']
                            testcases.append(test)
                            test = {}
    return testcases