Test Matrix Timeline
====================
CLI for backing up the test result in a local PostgreSQL database and visualize in a web page matrix layout.

* Preparing for Development

        * 1. Ensure ``pip`` and ``pipenv`` are installed
        * 2. Clone repository: ``git clone ``
        * 3. ``cd`` into repository
        * 4. Fetch development dependencies ``make install``
        * 5. Activate virtualenv: ``pipenv shell``
        * 6. pip install -e .


* Infrastructure

        * Make sure you have a postgres db installed 
                * To install a postgress db container, just run : db_setup_v04.sh

* Usage

        *  test_matrix --help
                *  Example: test_matrix -u postgres -f /tmp/output_TEF-VNS_20_10_R6_2021-10-06.xml -v no
       
