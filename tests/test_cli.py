import pytest
# import pdb
from test_matrix import cli

url = "postgres"

def test_parser_without_file_and_without_view():
 """
 Fail if the file and view is not specified
 """
 with pytest.raises(SystemExit):
 	parser = cli.create_parser()
 	parser.parse_args([url])
	
	
def test_parser_without_file():
 """
 Fail if the file option is not specified.
 """
 with pytest.raises(SystemExit):
 	parser = cli.create_parser()
 	parser.parse_args([url, "--view", "yes"])

def test_parser_without_view():
 """
 Fail if the file option is not specified.
 """
 with pytest.raises(SystemExit):
 	parser = cli.create_parser()
 	parser.parse_args([url, "--file", "/some/file/file.xml"])



def test_parser_with_unknown_view():
 """
 The parser will exit if the view is unknown.
 """
 parser = cli.create_parser()
 with pytest.raises(SystemExit):
 	parser.parse_args([url, '--file', '/some/path',  '--view', 'abc'])


def test_parser_with_all_parameters():
 """
 The parser will not exit if it receives a file
 """
 parser = cli.create_parser()
 args = parser.parse_args([url, "--file", '/some/file/file.txt', "--view", "yes"])
 assert args.url == url
 assert args.view == "yes"
#  assert args.file == '/some/file/file.txt'
